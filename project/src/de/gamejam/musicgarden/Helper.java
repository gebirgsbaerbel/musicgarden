package de.gamejam.musicgarden;

import java.util.Random;

public class Helper {
	static int clamp(int val, int b, int c) {
		int min, max;
		if(b >= c) {
			min = c;
			max = b;
		} else {
			min = b;
			max = c;
		}
		if(val < min) {
			return min;
		} else if (val > max) {
			return max;
		} else {
			return val;
		}
	}
	
	static float clamp(float val, float b, float c) {
		float min, max;
		if(b >= c) {
			min = c;
			max = b;
		} else {
			min = b;
			max = c;
		}
		if(val < min) {
			return min;
		} else if (val > max) {
			return max;
		} else {
			return val;
		}
	}
	
	static Random mRandom = null;
	static int rnd(int max) {
		if(mRandom == null)
			mRandom = new Random();
		return mRandom.nextInt(max);
	}
	
	static float rnd(float max) {
		if(mRandom == null)
			mRandom = new Random();
		return max * mRandom.nextFloat();
	}
	
	static float length(Vector2f v) {
		return (float) Math.sqrt(v.x*v.x + v.y*v.y);
	}
	
	static float getDistance(Vector2f a, Vector2f b) {
		return length(new Vector2f(a.x - b.x, a.y - b.y));
	}
	
	static float getDistance(Vector2i a, Vector2i b) {
		return length(new Vector2f(a.x - b.x, a.y - b.y));
	}
	
	static float getAngle(Vector2f a, Vector2f b) {
			float h = b.y - a.y;
			float l = b.x - a.x;
			float angle = (float)Math.atan(h/l);
			if(angle >= 0 && angle <= 90) { // sector ur
				angle = 90 - angle;
			} else if (angle > 90 && angle <= 180) { // sector ul
				angle = -(angle - 90);
			} else if (angle > 180 && angle <= 270) { // sector ll
				angle = -(angle - 90);
			} else { // sector lr
				angle = 450 - angle;
			}
			return angle;
	}
	
	static float getAngle(Vector2i a, Vector2i b) {
		return getAngle(a.toFloat(), b.toFloat());
	}
}
