package de.gamejam.musicgarden;

import java.util.concurrent.CopyOnWriteArrayList;

import de.gamejam.musicgarden.Flower.FlowerType;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.SurfaceView;

public class MusicGardenRenderer extends SurfaceView
{
	Bitmap mBitmapBackground;
	Bitmap mBitmapPlayer;
	
	Bitmap mBitmapFlowerLongSpriteSheet;
	Bitmap mBitmapFlowerLongSpriteSheetWithering;
	Bitmap mBitmapFlowerLongSpriteSheetAlmostDead;
	Bitmap mBitmapFlowerBeatSpriteSheet;
	Bitmap mBitmapFlowerBeatSpriteSheetWithering;
	Bitmap mBitmapFlowerBeatSpriteSheetAlmostDead;
	Bitmap mBitmapFlowerSingleSpriteSheet;
	Bitmap mBitmapFlowerSingleSpriteSheetWithering;
	Bitmap mBitmapFlowerSingleSpriteSheetAlmostDead;
	Bitmap mBitmapCloud1;
	Bitmap mBitmapCloud2;
	
	Vector2i mFlowerMeoldySize;
	Vector2i mFlowerLongSize;
	Vector2i mFlowerBeatSize;
	Vector2i mFlowerSingleSize;
	public static Vector2i mPlayerSize;
	
	int mCloudPos[][] = new int[4][2];
	float mCloudFactor[] = new float[4];

	private int mScreenWidth, mScreenHeight;
	public MusicGardenRenderer(Context context, int width, int height) {
		super(context);
		mScreenWidth = width;
		mScreenHeight = height;
		setWillNotDraw(false);
		
		for(int i = 0; i < 4; ++i) {
			mCloudPos[i][0] = Helper.rnd(mScreenWidth);
			mCloudPos[i][1] = Helper.rnd(50);
			mCloudFactor[i] = .5f + Helper.rnd(1.5f);
		}
		
		loadResources();
	}
	
	public void loadResources() {
		mBitmapBackground = BitmapFactory.decodeResource(getResources(), R.drawable.background);
		mBitmapPlayer = BitmapFactory.decodeResource(getResources(), R.drawable.player);
		mBitmapCloud1 = BitmapFactory.decodeResource(getResources(), R.drawable.cloud1);
		mBitmapCloud2 = BitmapFactory.decodeResource(getResources(), R.drawable.cloud2);

		mBitmapFlowerLongSpriteSheet = BitmapFactory.decodeResource(getResources(), R.drawable.flower_long);
		mBitmapFlowerLongSpriteSheetWithering = BitmapFactory.decodeResource(getResources(), R.drawable.flower_long_wither1);
		mBitmapFlowerLongSpriteSheetAlmostDead = BitmapFactory.decodeResource(getResources(), R.drawable.flower_long_wither2);
		
		mBitmapFlowerBeatSpriteSheet = BitmapFactory.decodeResource(getResources(), R.drawable.flower_beat);
		mBitmapFlowerBeatSpriteSheetWithering = BitmapFactory.decodeResource(getResources(), R.drawable.flower_beat_wither1);
		mBitmapFlowerBeatSpriteSheetAlmostDead = BitmapFactory.decodeResource(getResources(), R.drawable.flower_beat_wither2);
		
		mBitmapFlowerSingleSpriteSheet = BitmapFactory.decodeResource(getResources(), R.drawable.flower_single);
		mBitmapFlowerSingleSpriteSheetWithering = BitmapFactory.decodeResource(getResources(), R.drawable.flower_single_wither1);
		mBitmapFlowerSingleSpriteSheetAlmostDead = BitmapFactory.decodeResource(getResources(), R.drawable.flower_single_wither2);
		
		mFlowerLongSize = new Vector2i();
		mFlowerLongSize.x = mBitmapFlowerLongSpriteSheet.getWidth()/6;
		mFlowerLongSize.y = mBitmapFlowerLongSpriteSheet.getHeight()/6;
		mFlowerBeatSize = new Vector2i();
		mFlowerBeatSize.x = mBitmapFlowerBeatSpriteSheet.getWidth()/6;
		mFlowerBeatSize.y = mBitmapFlowerBeatSpriteSheet.getHeight()/6;
		mFlowerSingleSize = new Vector2i();
		mFlowerSingleSize.x = mBitmapFlowerSingleSpriteSheet.getWidth()/6;
		mFlowerSingleSize.y = mBitmapFlowerSingleSpriteSheet.getHeight()/6;
		
		mPlayerSize = new Vector2i();
		mPlayerSize.x = mBitmapPlayer.getWidth();
		mPlayerSize.y = mBitmapPlayer.getHeight();
	}

	RectF oval = new RectF();
	Paint waterPaint = new Paint();
	private void drawWatered(Canvas canvas, Flower f) {
		waterPaint.reset();
		waterPaint.setColor(0xff007fff);
		Vector2f vec = f.getPos();
		float lastWatered = f.getTimeSinceLastWatered();
		oval.left = vec.x - lastWatered * 25;
		oval.right = vec.x + lastWatered * 25;
		oval.top = vec.y + 50 - lastWatered * 20;
		oval.bottom = vec.y + 50 + lastWatered * 20;
		if(lastWatered > 0.f) {
			canvas.drawOval(oval, waterPaint);
		}
	}
	
	Paint p = new Paint();
	Rect src = new Rect();
	Rect dst = new Rect();
	public void onDraw(Canvas canvas) {
		p.reset();
		canvas.drawBitmap(mBitmapBackground, 0, 0, p);
		
		for(int i = 0; i < 4; ++i) {
			dst.top = mCloudPos[i][1];
			dst.left = mCloudPos[i][0];
			src.top = 0;
			src.left = 0;
			
			if((i % 2) == 0) {
				src.bottom = src.top + mBitmapCloud1.getHeight();
				src.right = src.left + mBitmapCloud1.getWidth();
				dst.bottom = dst.top + mBitmapCloud1.getHeight();
				dst.right = dst.left + mBitmapCloud1.getWidth();
				canvas.drawBitmap(mBitmapCloud1, src, dst, p);
			} else {
				src.bottom = src.top + mBitmapCloud2.getHeight();
				src.right = src.left + mBitmapCloud2.getWidth();
				dst.bottom = dst.top + mBitmapCloud2.getHeight();
				dst.right = dst.left + mBitmapCloud2.getWidth();
				canvas.drawBitmap(mBitmapCloud1, src, dst, p);
			}
		}
		
		for(int i = 0; i < mFlowersSingle.size(); ++i) {
			Flower f = mFlowersSingle.get(i);
			Bitmap b;
			Vector2i flowerSize = mFlowerSingleSize;

			drawWatered(canvas, f);
			
			switch (f.getState()) {
			case FLOWER_BLOOMING:
				b = mBitmapFlowerSingleSpriteSheet;
				break;
			case FLOWER_WITHERING:
				b = mBitmapFlowerSingleSpriteSheetWithering;
				break;
			case FLOWER_NEAR_DEAD:
				b = mBitmapFlowerSingleSpriteSheetAlmostDead;
				break;
			case FLOWER_DEAD:
				b = mBitmapFlowerSingleSpriteSheetAlmostDead;
				break;
			default:
				b = mBitmapFlowerSingleSpriteSheetAlmostDead;
				break;
			}
			
			Vector2i state = f.getDrawState();
			src.top = state.y * flowerSize.y;
			src.bottom = src.top + flowerSize.y;
			src.left = state.x * flowerSize.x;
			src.right = src.left + flowerSize.x;
			
			Vector2i pos = f.getPos().toInt();
			dst.top = pos.y - flowerSize.y/2;
			dst.bottom = dst.top + flowerSize.y;
			dst.left = pos.x - flowerSize.x/2;
			dst.right = dst.left + flowerSize.x;

			canvas.drawBitmap(b, src, dst, p);
		}
		
		for(int i = 0; i < mFlowersBeat.size(); ++i) {
			Flower f = mFlowersBeat.get(i);
			Bitmap b;
			Vector2i flowerSize = mFlowerBeatSize;
			
			drawWatered(canvas, f);

			switch (f.getState()) {
			case FLOWER_BLOOMING:
				b = mBitmapFlowerBeatSpriteSheet;
				break;
			case FLOWER_WITHERING:
				b = mBitmapFlowerBeatSpriteSheetWithering;
				break;
			case FLOWER_NEAR_DEAD:
				b = mBitmapFlowerBeatSpriteSheetAlmostDead;
				break;
			case FLOWER_DEAD:
				b = mBitmapFlowerBeatSpriteSheetAlmostDead;
				break;
			default:
				b = mBitmapFlowerBeatSpriteSheetAlmostDead;
				break;
			}
			
			Vector2i state = f.getDrawState();
			src.top = state.y * flowerSize.y;
			src.bottom = src.top + flowerSize.y;
			src.left = state.x * flowerSize.x;
			src.right = src.left + flowerSize.x;
			
			Vector2i pos = f.getPos().toInt();
			dst.top = pos.y - flowerSize.y/2;
			dst.bottom = dst.top + flowerSize.y;
			dst.left = pos.x - flowerSize.x/2;
			dst.right = dst.left + flowerSize.x;

			canvas.drawBitmap(b, src, dst, p);
		}
		
		for(int i = 0; i < mFlowersLong.size(); ++i) {
			Flower f = mFlowersLong.get(i);
			Bitmap b;
			Vector2i flowerSize = mFlowerLongSize;
			
			drawWatered(canvas, f);
			
			switch (f.getState()) {
			case FLOWER_BLOOMING:
				b = mBitmapFlowerLongSpriteSheet;
				break;
			case FLOWER_WITHERING:
				b = mBitmapFlowerLongSpriteSheetWithering;
				break;
			case FLOWER_NEAR_DEAD:
				b = mBitmapFlowerLongSpriteSheetAlmostDead;
				break;
			case FLOWER_DEAD:
				b = mBitmapFlowerLongSpriteSheetAlmostDead;
				break;
			default:
				b = mBitmapFlowerLongSpriteSheetAlmostDead;
				break;
			}
			
			Vector2i state = f.getDrawState();
			src.top = state.y * flowerSize.y;
			src.bottom = src.top + flowerSize.y;
			src.left = state.x * flowerSize.x;
			src.right = src.left + flowerSize.x;
			
			Vector2i pos = f.getPos().toInt();
			dst.top = pos.y - flowerSize.y/2;
			dst.bottom = dst.top + flowerSize.y;
			dst.left = pos.x - flowerSize.x/2;
			dst.right = dst.left + flowerSize.x;

			canvas.drawBitmap(b, src, dst, p);
		}
		
		Vector2i pos = mPlayer.getPos().toInt();
		int w = mBitmapPlayer.getWidth();
		int h = mBitmapPlayer.getHeight();
		src.top = 0;
		src.left = 0;
		src.right = src.left + w;
		src.bottom = src.top + h;
		dst.top = pos.y - w/2;
		dst.left = pos.x - h/2;
		dst.right = dst.left + w;
		dst.bottom = dst.top + h;	
		canvas.drawBitmap(mBitmapPlayer, src, dst, p);
	}
	
	public void updateClouds(float dt) {
		for(int i = 0; i < 4; ++i) {
			mCloudPos[i][0] += 5 + dt * mCloudFactor[i] * 50;
			if(mCloudPos[i][0] > mScreenWidth)
				mCloudPos[i][0] = -100;
		}
	}

	private Player mPlayer;
	private CopyOnWriteArrayList<Flower> mFlowersSingle, mFlowersLong, mFlowersBeat;
	public void setObjects(Player player, CopyOnWriteArrayList<Flower> flowersSingle
			, CopyOnWriteArrayList<Flower> flowersLong, CopyOnWriteArrayList<Flower> flowersBeat) {
		mPlayer = player;
		mFlowersSingle = flowersSingle;
		mFlowersLong = flowersLong;
		mFlowersBeat = flowersBeat;
	}
}
