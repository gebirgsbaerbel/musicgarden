package de.gamejam.musicgarden;

public class BaseObject {
	protected float mPosX;
	protected float mPosY;
	
	public Vector2f getPos() {
		return new Vector2f(mPosX, mPosY);
	}
	
	BaseObject(float x, float y) {
		mPosX = x;
		mPosY = y;
	}
}
