package de.gamejam.musicgarden;

import android.util.Log;

public class Flower extends BaseObject {
	enum FlowerType {
		FLOWER_BEAT,
		FLOWER_LONG,
		FLOWER_SINGLE
	};
	
	enum FlowerState {
		FLOWER_BLOOMING,
		FLOWER_WITHERING,
		FLOWER_NEAR_DEAD,
		FLOWER_DEAD
	};

	private FlowerType mType;
	private FlowerState fstate;
	private float mTime;
	private final static float TIME_SLICE = .05f;
	private final static float MAX_LIFESPAN = 10.f;
	private float lifeSpanInSeconds;
	private float mTimeLastWatered = 0.f;
	Flower(float x, float y, FlowerType t) {
		super(x, y);
		mType = t;
		mState = new Vector2i();
		mState.x = Helper.rnd(6);
		mState.y = Helper.rnd(6);
		mTime = Helper.rnd(TIME_SLICE);
		lifeSpanInSeconds = 10;
		fstate = FlowerState.FLOWER_BLOOMING;
	}
	
	public FlowerType getType() {
		return mType;
	}
	
	void update(float dt) {
		mTime += dt;
		if(mTime > TIME_SLICE) {
			mState.x++;
			if(mState.x == 6) {
				mState.x = 0;
				mState.y++;
				if(mState.y == 6) {
					mState.y = 0;
				}
			}
			mTime -= TIME_SLICE;
			
			//withering of the plant
			lifeSpanInSeconds -= dt;
			if (lifeSpanInSeconds <= 3 && lifeSpanInSeconds > 1) {
				fstate = FlowerState.FLOWER_WITHERING;
			} else if (lifeSpanInSeconds <= 1 && lifeSpanInSeconds > 0) {
				fstate = FlowerState.FLOWER_NEAR_DEAD;
			} else if (lifeSpanInSeconds <= 0) {
				fstate = FlowerState.FLOWER_DEAD;
			}
		}
		
		if(mTimeLastWatered > 0.f)
			mTimeLastWatered -= dt;
	}
	
	FlowerState getState() {
		return fstate;
	}
	
	private Vector2i mState;
	Vector2i getDrawState() {
		return mState;
	}
	
	float getLifespaceNormalised() {
		return lifeSpanInSeconds/MAX_LIFESPAN;
	}
	
	public void water() {
		mTimeLastWatered = 1.5f;
		lifeSpanInSeconds += MAX_LIFESPAN/3;
	}
	
	public float getTimeSinceLastWatered() {
		if(mTimeLastWatered < 0.f)
			return 0.f;
		return mTimeLastWatered;
	}
}