package de.gamejam.musicgarden.microphone;

import java.util.concurrent.Semaphore;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioRecord.OnRecordPositionUpdateListener;
import android.media.MediaRecorder.AudioSource;
import android.util.Log;

public class MicrophoneThread {
	private static final int SAMPLE_RATE = 8000;
	private static final int CHANNEL_CONFIG = AudioFormat.CHANNEL_CONFIGURATION_MONO;
	private static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
	
	private int bufferSize = 800;
	private short[][] buffers = new short[256][bufferSize];
	private int lastBuffer = 0;
	private AudioRecord recorder;
	
	long timeOfLastPlantedFlower;
	
	private MicrophoneListener listener;
	
	public MicrophoneThread(MicrophoneListener listener) {
		this.listener = listener;
	}
	
	public void init() {
		timeOfLastPlantedFlower = 0; 
		
		int minBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT);
		recorder = new AudioRecord(AudioSource.MIC, SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT, minBufferSize * 10);
		recorder.setPositionNotificationPeriod(bufferSize);
		recorder.setRecordPositionUpdateListener(new OnRecordPositionUpdateListener() {
			@Override
			public void onPeriodicNotification(AudioRecord recorder) {
				short[] buffer = buffers[++lastBuffer % buffers.length];
				recorder.read(buffer, 0, bufferSize);
				long sum = 0;
				for (int i = 0; i < bufferSize; ++i) {
					sum += Math.abs(buffer[i]);
				}
				int volume = (int) (sum / bufferSize);
				if (System.currentTimeMillis() - timeOfLastPlantedFlower > 1000) {
					boolean plantedFlower = false;
					if (volume > 1800 && volume < 4000) {
						listener.silentSound();
						plantedFlower = true;
					} else if (volume >= 4000 && volume < 9000) {
						listener.mediumSound();
						plantedFlower = true;
					} else if (volume >= 9000) {
						listener.loudSound();
						plantedFlower = true;
					}
					if (plantedFlower) {
						timeOfLastPlantedFlower = System.currentTimeMillis();
					}
				}
				lastBuffer = lastBuffer % buffers.length;
			}

			@Override
			public void onMarkerReached(AudioRecord recorder) {
			}
		});
		recorder.startRecording();
		short[] buffer = buffers[lastBuffer % buffers.length];
		recorder.read(buffer, 0, bufferSize);

	}
	
	public void close() {
//		while (true) {
//		if (isInterrupted()) {
			recorder.stop();
			recorder.release();
//			break;
//		}
//		try {
//			Thread.sleep(10);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	}
}
