package de.gamejam.musicgarden.microphone;

public interface MicrophoneListener {
	
	void silentSound();
	void mediumSound();
	void loudSound();
	
}
