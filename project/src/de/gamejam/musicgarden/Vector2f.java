package de.gamejam.musicgarden;

public class Vector2f {
	public float x;
	public float y;
	
	
	Vector2f() {
		
	}
	
	Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	Vector2i toInt() {
		Vector2i a = new Vector2i();
		a.x = (int) x;
		a.y = (int) y;
		return a;
	}
}
