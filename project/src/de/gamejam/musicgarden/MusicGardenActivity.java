package de.gamejam.musicgarden;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import org.puredata.android.io.PdAudio;
import org.puredata.core.PdBase;

import de.gamejam.musicgarden.Flower.FlowerState;
import de.gamejam.musicgarden.microphone.MicrophoneListener;
import de.gamejam.musicgarden.microphone.MicrophoneThread;

import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Debug;
import android.app.Activity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.support.v4.app.NavUtils;

public class MusicGardenActivity extends Activity implements SensorEventListener, MicrophoneListener {
	private final static boolean CREATE_TRACE = false;
	private final static int SAMPLE_RATE = 44100;
	private final static int TARGET_FRAME_RATE = 30;
	private final static int SPAWN_MIN_IN_SECONDS = 2;
	private MusicGardenRenderer mMusicGardenRenderer;
	private Thread mThread;
	private MicrophoneThread microphoneThread;
	private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private float mGyroscopeState[] = new float[2];
    private float mScreenDiagLength;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if(CREATE_TRACE)
    		Debug.startMethodTracing("mga_trace");
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        mMusicGardenRenderer = new MusicGardenRenderer(this, display.getWidth(), display.getHeight());
        setContentView(mMusicGardenRenderer);
        microphoneThread = new MicrophoneThread(this);
        microphoneThread.init();
        try {
			PdAudio.initAudio(SAMPLE_RATE, 0, 2, 128, true);
			PdBase.openPatch("/sdcard/musicgarden/GJ220912_Mixer.pd");
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mScreenDiagLength = Helper.length(new Vector2f(display.getWidth(), display.getHeight()));
        for(int i = 0; i < mSoundLongSend.length; ++i)
        	mSoundLongSend[i] = false;
        
        mThread = new Thread(new Runnable()
		{
			public void run()
	        {
				update();
	        }
		});
		mThread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_music_garden, menu);
        return true;
    }

    @Override
    protected void onPause() {
    	PdAudio.stopAudio();
    	mSensorManager.unregisterListener(this);
//    	microphoneThread.onPause();
    	super.onPause();
    	if(CREATE_TRACE)
    		Debug.stopMethodTracing();
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	if(event.getAction() == MotionEvent.ACTION_DOWN) {
    		for(int i = 0; i < mFlowersBeat.size(); ++i) {
    			if(Helper.getDistance(mPlayer.getPos(), mFlowersBeat.get(i).getPos()) < 30) {
    				mFlowersBeat.get(i).water();
    			}
    			
    		}
    		for(int i = 0; i < mFlowersLong.size(); ++i) {
    			if(Helper.getDistance(mPlayer.getPos(), mFlowersLong.get(i).getPos()) < 30) {
    				mFlowersLong.get(i).water();
    			}
    			
    		}
    		for(int i = 0; i < mFlowersSingle.size(); ++i) {
    			if(Helper.getDistance(mPlayer.getPos(), mFlowersSingle.get(i).getPos()) < 30) {
    				mFlowersSingle.get(i).water();
    			}
    			
    		}
    		return true;
    	}
    	return super.onTouchEvent(event);
    }
    
    @Override
    protected void onResume() {
    	PdAudio.startAudio(this);
    	mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
//    	microphoneThread.onResume();
    	super.onResume();
    }
    
    @Override
    protected void onDestroy() {
    	microphoneThread.close();
    	super.onDestroy();
    }
    
    private Player mPlayer;
    private CopyOnWriteArrayList<Flower> mFlowersSingle, mFlowersLong, mFlowersBeat;
    private void initGame() {
    	Display display = getWindowManager().getDefaultDisplay();
    	mPlayer = new Player(50, 50, display.getWidth(), display.getHeight());
    	mFlowersSingle = new CopyOnWriteArrayList<Flower>();
    	mFlowersLong = new CopyOnWriteArrayList<Flower>();
    	mFlowersBeat = new CopyOnWriteArrayList<Flower>();
    	
//    	mFlowersBeat.add(new Flower(150, 450, Flower.FlowerType.FLOWER_BEAT));
//    	mFlowersSingle.add(new Flower(450, 150, Flower.FlowerType.FLOWER_SINGLE));
//    	mFlowersLong.add(new Flower(450, 450, Flower.FlowerType.FLOWER_LONG));
    	
    	mMusicGardenRenderer.setObjects(mPlayer, mFlowersSingle, mFlowersLong, mFlowersBeat);
    }
    
    private void update() {
    	long currentTimeMillis = System.currentTimeMillis();
    	long now;
    	boolean run = true;
    	float dt = 0.f;
    	initGame();
    	while(run) {
    		
	    	// compute all updates
    		mPlayer.update(dt, mGyroscopeState);
    		for(int i = 0; i < mFlowersSingle.size(); ++i) {
    			mFlowersSingle.get(i).update(dt);
    			if (((Flower)mFlowersSingle.get(i)).getState() == FlowerState.FLOWER_DEAD) {
    				mFlowersSingle.remove(i);
    				PdBase.sendFloat(String.valueOf(i + 2) + "We", 0.f);
    			}
    		}
    		for(int i = 0; i < mFlowersLong.size(); ++i) {
    			mFlowersLong.get(i).update(dt);
    			if (((Flower)mFlowersLong.get(i)).getState() == FlowerState.FLOWER_DEAD) {
    				mFlowersLong.remove(i);
    				PdBase.sendFloat(String.valueOf(i + 8) + "We", 0.f);
    			}
    		}
    		for(int i = 0; i < mFlowersBeat.size(); ++i) {
    			mFlowersBeat.get(i).update(dt);
    			if (((Flower)mFlowersBeat.get(i)).getState() == FlowerState.FLOWER_DEAD) {
    				mFlowersBeat.remove(i);
    				PdBase.sendFloat("1We", 0.f);
    				mSoundBeatSend = false;
    			}
    		}
    		mMusicGardenRenderer.updateClouds(dt);
    		
    		// update audio
    		updateMusic();
    		
    		// create random flowers
    		createRandomFlowers(dt);
	    	
	    	// request redraw
	    	mMusicGardenRenderer.postInvalidate();
	    	now = System.currentTimeMillis();
			long millis = 1000/TARGET_FRAME_RATE - (now - currentTimeMillis);
			if(millis < 1 || millis > 34)
				millis = 15;
			try
	        {
	            Thread.sleep(millis, 0);
	        } catch(InterruptedException e) { }
			now = System.currentTimeMillis();
			dt = (now - currentTimeMillis)/1000.f;
	        currentTimeMillis = now;
    	}
    }
    
    private float timeToSpawn;
    private int nextQuad;
    private void createRandomFlowers(float dt) {
    	timeToSpawn -= dt;
    	if(timeToSpawn <= 0) {
    		Display display = getWindowManager().getDefaultDisplay();
    		int x = 0, y = 0;
    		if(nextQuad == 0) {
    			x = 0;
    			y = 0;
    		} else if (nextQuad == 1) {
    			x = display.getWidth()/2;
    			y = 0;
    		} else if (nextQuad == 2) {
    			x = 0;
    			y = display.getHeight()/2;
    		} else {
    			x = display.getWidth()/2;
    			y = display.getHeight()/2;
    		}
    		
    		x += Helper.rnd(display.getWidth()/2);
    		y += 100 + Helper.rnd((display.getHeight() - 100)/2);
    		
    		switch(Helper.rnd(10))
    		{
	    		case 2:
	    			mFlowersBeat.add(new Flower(x, y, Flower.FlowerType.FLOWER_BEAT));
	    			break;
	    		case 3:
	    		case 5:
	    		case 9:
	    			mFlowersSingle.add(new Flower(x, y, Flower.FlowerType.FLOWER_SINGLE));
	    			break;
				default:
					mFlowersLong.add(new Flower(x, y, Flower.FlowerType.FLOWER_LONG));
					break;
    		}
    		nextQuad = (nextQuad + 1) % 4;
    		timeToSpawn = SPAWN_MIN_IN_SECONDS + Helper.rnd(3);
    	}
		
	}

	boolean mSoundBeatSend = false;
    boolean mSoundLongSend[] = new boolean[10];
    public void updateMusic() {
    	Flower f;
    	float distNearest, dist;
    	if(mFlowersBeat.size() > 0) {
    		f = mFlowersBeat.get(0);
    		distNearest = Helper.getDistance(f.getPos(), mPlayer.getPos());
    		for(int i = 1; i < mFlowersBeat.size(); ++i) {
    			dist = Helper.getDistance(mFlowersBeat.get(i).getPos(), mPlayer.getPos()); 
    			if(dist < distNearest) {
    				f = mFlowersBeat.get(i);
    				distNearest = dist;
    			}
    		}
    		if(!mSoundBeatSend) {
    			mSoundBeatSend = true;
    			PdBase.sendBang("1");
    		}
    		PdBase.sendFloat("1Wi", Helper.getAngle(mPlayer.getPos(), f.getPos()));
    		PdBase.sendFloat("1Ab", distNearest/mScreenDiagLength);
    		PdBase.sendFloat("1We", f.getLifespaceNormalised());
    	}
    	
    	for(int i = 0; i < Math.min(mFlowersSingle.size(), 6); ++i) {
    		f = mFlowersSingle.get(i);
    		dist = Helper.getDistance(f.getPos(), mPlayer.getPos());
    		if(dist < 30) {
    			PdBase.sendBang(String.valueOf(i + 2));
    		}
    		PdBase.sendFloat(String.valueOf(i + 2) + "Wi", Helper.getAngle(mPlayer.getPos(), f.getPos()));
    		PdBase.sendFloat(String.valueOf(i + 2) + "Ab", dist/mScreenDiagLength);
    		PdBase.sendFloat(String.valueOf(i + 2) + "We", f.getLifespaceNormalised());
    	}
    	
    	for(int i = 0; i < Math.min(mFlowersLong.size(), 10); ++i) {
    		f = mFlowersLong.get(i);
    		dist = Helper.getDistance(f.getPos(), mPlayer.getPos());
    		if(!mSoundLongSend[i]) {
    			mSoundLongSend[i] = true;
    			PdBase.sendBang(String.valueOf(i + 8));
    		}
    		PdBase.sendFloat(String.valueOf(i + 8) + "Wi", Helper.getAngle(mPlayer.getPos(), f.getPos()));
    		PdBase.sendFloat(String.valueOf(i + 8) + "Ab", dist/mScreenDiagLength);
    		PdBase.sendFloat(String.valueOf(i + 8) + "We", f.getLifespaceNormalised());
    	}
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// not expected to happen :)
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE && event.values.length >= 2) {
			mGyroscopeState[0] += event.values[0];
			mGyroscopeState[1] -= event.values[1];
			mGyroscopeState[0] = Helper.clamp(mGyroscopeState[0], -20, 20);
			mGyroscopeState[1] = Helper.clamp(mGyroscopeState[1], -20, 20);
		}
		
	}

	@Override
	public void silentSound() {
		Vector2f playerPosition = mPlayer.getPos();
		mFlowersSingle.add(new Flower(playerPosition.x, playerPosition.y, Flower.FlowerType.FLOWER_SINGLE));
	}

	@Override
	public void mediumSound() {
		Vector2f playerPosition = mPlayer.getPos();
		mFlowersLong.add(new Flower(playerPosition.x, playerPosition.y, Flower.FlowerType.FLOWER_LONG));
	}

	@Override
	public void loudSound() {
		Vector2f playerPosition = mPlayer.getPos();
		mFlowersBeat.add(new Flower(playerPosition.x, playerPosition.y, Flower.FlowerType.FLOWER_BEAT));
	}
}
