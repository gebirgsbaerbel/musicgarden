package de.gamejam.musicgarden;

import android.util.Log;
import android.view.Display;

public class Player extends BaseObject {
	private int mScreenWidth, mScreenHeight;
	Player(float x, float y, int screenWidth, int screenHeight) {
		super(x, y);
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		
	}
	
	void translate(float x, float y) {
		mPosX += x;
		mPosY += y;
		mPosX = Helper.clamp(mPosX, MusicGardenRenderer.mPlayerSize.x/2+10, mScreenWidth-MusicGardenRenderer.mPlayerSize.x/2-10);
		mPosY = Helper.clamp(mPosY, MusicGardenRenderer.mPlayerSize.y * 1.2f, mScreenHeight - MusicGardenRenderer.mPlayerSize.y/2 - 15);
	}

	public void update(float dt, float[] gyroscopeState) {
		translate(10.f * dt * gyroscopeState[0], 10.f * dt * gyroscopeState[1]);
		
	}
}
