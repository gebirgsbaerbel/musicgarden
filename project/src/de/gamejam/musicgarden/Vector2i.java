package de.gamejam.musicgarden;

public class Vector2i {
	public int x;
	public int y;
	
	Vector2i() {
		
	}
	
	Vector2i(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	Vector2f toFloat() {
		Vector2f a = new Vector2f();
		a.x = x;
		a.y = y;
		return a;
	}
}
